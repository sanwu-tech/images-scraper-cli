'use strict'

const PROVIDERS = [
	'bing',
	'google',
	'yahoo',
]

let fs = require('fs')
let path = require('path')
let process = require('process')
let url = require('url')

let request = require('request')
let mkdirp = require('mkdirp')
let program = require('commander')
let Scraper = require('images-scraper')
let provider

program
	.version('0.0.1')
	.option('-p, --provider [PROVIDER]', 'images source (default: google)')
	.option('-n, --num-images [NUM]', 'number of images to retrieve (default: 100)', parseInt)
	.option('-t, --timeout [MS]', 'set connection timeout', parseInt)
	.parse(process.argv)

if (typeof(program.numImages) === 'number' && isNaN(program.numImages)) {
	console.error('error: --num-images value is invalid')
	return
} else if (typeof(program.numImages) === 'undefined'){
	program.numImages = 100
}

if (typeof(program.timeout) === 'number' && isNaN(program.timeout)) {
	console.error('error: --timeout value is invalid')
	return
} else if (typeof(program.timeout) === 'undefined'){
	program.timeout = 3000
}

if (typeof(program.provider) === 'string' && PROVIDERS.indexOf(program.provider) === -1) {
	console.error('error: --provider value must be one of theses:', PROVIDERS.toString())
	return
} else if (typeof(program.provider) === 'undefined') {
	program.provider = 'google'
}

if (program.args.length === 0) {
	console.log('please specify the search text')
	return
}

for (let i in program.args) {
	let arg = program.args[i]

	console.log('looking for', program.numImages, arg, 'images from', program.provider)

	if (program.provider === 'google')
		provider = new Scraper.Google()
	else if (program.provider === 'bing')
		provider = new Scraper.Bing()
	else if (program.provider === 'yahoo')
		provider = new Scraper.Yahoo()

	provider.list({
		keyword: arg,
		num: program.numImages,
		detail: true,
		nightmare: {
			show: true,
		},
	}).then(function(results) {
		let dirpath = __dirname + '/results/' + arg
		downloadAll(results, dirpath)
	}).catch(function(error) {
		console.error('error:', error)
	})
}

var download = function(uri, dirpath, filename, callback) {
	request({ uri: uri, method: 'HEAD', timeout: program.timeout }, function(error, res, body) {
		if (error) {
			callback(error)
			return
		}

		mkdirp(dirpath, function(error) {
			if (error) {
				console.log('error:', error)
				callback(error)
				return
			}

			request(uri).pipe(fs.createWriteStream(dirpath + '/' + filename))
				.on('error', callback)
				.on('close', callback)
		})
	})
}

var downloadAll = function(images, dirpath) {
	let i = 0

	if (images.length === 0)
		return

	let callback = function(error) {
		if (error)
			console.error('failed to download', images[i].url)
		else
			console.log('[' + (i + 1) + '/' + images.length + '] saved', images[i].url)

		i += 1

		if (images.length > i)
			download(images[i].url, dirpath, path.basename(images[i].url), callback)
	}

	download(images[i].url, dirpath, path.basename(images[i].url), callback)
}
